bool updateReadings() {
  if (airSensor.dataAvailable()) {
    // Read the sensor
    co2 = airSensor.getCO2();
    t = airSensor.getTemperature();
    h = airSensor.getHumidity();
    return true;
  }
  return false;
}

void calibration() {
  // Display menu asking to confirm calibration
  displayCalibration1();
  M5.update();
  while(!M5.BtnUP.wasPressed() && !M5.BtnMID.isPressed()) {
    M5.update();
    delay(100);
  }

  // If mid button was pressed we can go on with the calibration process
  if (M5.BtnMID.isPressed()) {

    // Wait 200s for the sensor to aclimate
    int calibrationCounter = 200;
    while(calibrationCounter >= 0) {
      displayCalibration2(calibrationCounter);
      updateReadings();
      delay(1000);
      calibrationCounter--;
    }

    // Send the forces calibration command
    airSensor.setForcedRecalibrationFactor(CALIBRATION_VALUE);
    updateReadings();
    delay(500);

    // Wait for a first valid reading
    while(!updateReadings()) {
      delay(1000);
    }
  }
}
