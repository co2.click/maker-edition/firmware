float decimalRound(float input, int decimals) {
  float scale=pow(10,decimals);
  return round(input*scale)/scale;
}
