#define ENABLE_GxEPD2_GFX 0
#define GxEPD2_DRIVER_CLASS GxEPD2_154_M09
#define DISABLE_DIAGNOSTIC_OUTPUT false

#include <GxEPD2_BW.h>
#include <Fonts/FreeMono9pt7b.h>
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeMonoBold18pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include "images.h"

GxEPD2_154_M09 medp = GxEPD2_154_M09(/*CS=D8*/ 9, /*DC=D3*/ 15, /*RST=D4*/ 0, /*BUSY=D2*/ 4);
GxEPD2_BW<GxEPD2_154_M09, GxEPD2_154_M09::HEIGHT> display(medp);  // GDEH0154D67

char buffer[64];

void displayInit() {
  display.init(0,false);
  displayClear();
}

void displayCO2ValuesPartialMode() {
  display.setFullWindow();
  display.setRotation(0);
  display.setTextColor(GxEPD_BLACK);
  display.fillScreen(GxEPD_WHITE);

  uint16_t y;
  display.fillScreen(GxEPD_WHITE);

  // Display the CO2 ppm value
  display.setTextSize(2);
  display.setFont(&FreeMonoBold18pt7b);
  if(co2 > 9999) {
    display.setFont(&FreeMonoBold12pt7b);
  }
  sprintf(buffer, "%i",co2);
  positionString(buffer, 0,(display.height() / 2 - 30));
  display.print(buffer);

  // Display the small ppm label
  display.setTextSize(0);
  display.setFont(&FreeMonoBold9pt7b);
  display.setCursor((display.width() / 2 - 14), (display.height() / 2 - 8));
  display.print("PPM");

  // Display the rebreathed fraction of air
  display.setFont(&FreeMono9pt7b);
  float rfa = ((co2-415)/38000.0);
  sprintf(buffer, "RFA: %0.1f%% (%0.0fL/h)",rfa*100,416.67*rfa);
  positionString(buffer, 0,(display.height() / 2)+15);
  display.print(buffer);

  // Display the temperature and humidity
  display.setFont(&FreeMonoBold12pt7b);
  display.setTextSize(1);
  y = display.height() / 2 + 50;
  display.setCursor(0, y);
  if(units) {
    display.printf("%.1f",t);
  } else {
    display.printf("%.1f",((t*1.8)+32));
  }
  sprintf(buffer,"%d   ",h);
  positionStringLeft(buffer, y);
  display.printf("%d",h);

  displayIcons();

  fullUpdateWait();
}

void displayCalibration1() {
  fullUpdateSetup();
  positionString("Calibration", 0,12);
  display.println("Calibration\n");
  display.println("Bring the sensor");
  display.println("outside and press");
  display.println("the button to");
  display.println("start.");
  display.println("");
  display.println("Up = exit");
  fullUpdateWait();
}

void displayCalibration2(int count) {
  fullUpdateSetup();
  positionString("Calibration", 0,12);
  display.println("Calibration\n");
  display.println("Calibration in");
  display.println("progress...");
  display.setTextSize(2);
  sprintf(buffer,"%d",count);
  positionString(buffer, 0,display.getCursorY()+10);
  display.printf("%d",count);
  display.setTextSize(1);
  display.println("\n\nSensor will return");
  display.println("to the main screen");
  display.println("when completed.");
  fullUpdateWait();
}


void displayIcons() {
  if(units) {
    display.drawBitmap( 60, 130, celcius, 24, 24, GxEPD_WHITE,GxEPD_BLACK);
  } else {
     display.drawBitmap( 60, 130, fahrenheit, 24, 24, GxEPD_WHITE,GxEPD_BLACK);
  }
  display.drawBitmap(display.width()-24, 130, humidity, 24, 24, GxEPD_WHITE,GxEPD_BLACK);
}

void displayHome() {
  fullUpdateSetup();
  display.setFont(&FreeMonoBold12pt7b);
  display.drawBitmap (34, 10, logo, 132, 62, GxEPD_WHITE,GxEPD_BLACK);
  positionString("co2.click", 0,90);
  display.println("co2.click");
  display.setFont(&FreeMonoBold9pt7b);
  positionString("v"+String(VERSION_STRING),0,180);
  display.println("v"+String(VERSION_STRING));
  fullUpdateWait();
}

void displayClear() {
  display.fillScreen(GxEPD_WHITE);
}

void positionString(String text, uint16_t x, uint16_t y) {
  int16_t tbx, tby;
  uint16_t tbw, tbh;
  display.getTextBounds(text, 0, 0, &tbx, &tby, &tbw, &tbh);
  if(x == 0) {
    x = ((display.width() - tbw) / 2) - tbx;
  }
  if(y == 0) {
    y = ((display.height() - tbh) / 2) - tby;
  }
  display.setCursor(x, y);
}

void positionStringLeft(String text, uint16_t y) {
  int16_t tbx, tby;
  uint16_t tbw, tbh, x;
  display.getTextBounds(text, 0, 0, &tbx, &tby, &tbw, &tbh);
  x = (display.width() - tbw)- tbx;
  display.setCursor(x, y);
}

void fullUpdateSetup() {
  display.setFullWindow();
  display.setRotation(0);
  display.setFont(&FreeMonoBold9pt7b);
  display.setTextSize(1);
  display.setTextColor(GxEPD_BLACK);
  display.fillScreen(GxEPD_WHITE);
}

void fullUpdateWait() {
  while (display.nextPage ());
  display.powerOff();
}
