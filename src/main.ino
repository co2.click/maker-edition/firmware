#include <SparkFun_SCD30_Arduino_Library.h>
#include <Wire.h>
#include <M5CoreInk.h>
#include <SPI.h>

#define VERSION_STRING   "1.1.1"

// Buttons
#define UP_BUTTON GPIO_NUM_37
#define DOWN_BUTTON GPIO_NUM_39
#define PRESS_BUTTON GPIO_NUM_38

// SCD30
SCD30 airSensor;
bool scd30Present = false;

// This is the value that will be used for forces calibration
#define CALIBRATION_VALUE 415

// Globals holding the data
uint8_t h = 0;
float t = 0.0;
uint16_t co2 = 0;
bool units = true;  // true = Celcius, false = Fahrenheit

// Initial setup
void setup()
{

  // Enable serial port for debugging
  Serial.begin(115200);

  // Turn off the module green LED
  pinMode(LED_EXT_PIN, OUTPUT);
  digitalWrite(LED_EXT_PIN, HIGH);

  // Initialising the OLED display and M5Stack object
  M5.begin(true, true, false);
  if( !M5.M5Ink.isInit())
  {
    while (1) {
      digitalWrite(LED_EXT_PIN, LOW);
      delay(500);
      digitalWrite(LED_EXT_PIN, HIGH);
      delay(500);
    }
  }

  // Boot splashpage
  displayInit();
  displayHome();

  // SCD30
  Wire.begin(22, 21);
  if (airSensor.begin() == false) {
    while(true) {
      digitalWrite(LED_EXT_PIN, LOW);
      delay(1000);
      digitalWrite(LED_EXT_PIN, HIGH);
      delay(1000);
    }
  } else {
    airSensor.beginMeasuring();
  }

  // Wait for a first valid reading
  delay(3000);
  while(!updateReadings()) {
    delay(1000);
  }

  // Do a first display update
  displayCO2ValuesPartialMode();

}


void loop() {

  // Sleeping for 30 seconds
  esp_sleep_enable_timer_wakeup((30)*1000000);
  gpio_wakeup_enable(UP_BUTTON, GPIO_INTR_LOW_LEVEL);
  gpio_wakeup_enable(DOWN_BUTTON, GPIO_INTR_LOW_LEVEL);
  gpio_wakeup_enable(PRESS_BUTTON, GPIO_INTR_LOW_LEVEL);
  esp_sleep_enable_gpio_wakeup();
  esp_light_sleep_start();

  // Get button status update
  M5.update();

  if(M5.BtnMID.wasPressed() || !digitalRead(PRESS_BUTTON)) {
    // Button press
  }
  else if(M5.BtnDOWN.wasPressed() || !digitalRead(DOWN_BUTTON)) {
    // Button down
    calibration();
  }
  else if(M5.BtnUP.wasPressed() || !digitalRead(UP_BUTTON)) {
    // Button up
  }

  // Get a reading from the sensor
  updateReadings();

  // Update the display
  displayCO2ValuesPartialMode();

}




