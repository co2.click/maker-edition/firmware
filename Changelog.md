# Changelog

All notable changes to this project will be documented in this file.

NOTE: the project follows [Semantic Versioning](http://semver.org/).

## v1.1.1 - January 1st, 2023

- [bug] #3 - Temperature unit logic is reversed

## v1.1.0 - October 6th, 2022

- [enhancement] #2 - Add calibration feature

## v1.0.0 - September 30th, 2022

- Inivial version

