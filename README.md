# co2.click Maker-Edition CO2 sensor firmware

## Description

This is the base firmware for the co2.click Maker-Edition CO2 sensor

## Build

### Requirements

- Platformio

### Building the firmware for Linux/macOS

As simple as `script/build [only|erase|monitor]`.

- When running `script/build` without any options a build will be performed and attempt to upload it to a connected device
- `only` will just build the firmware
- `erase` will erase the connected device flash
- `monitor` will build and upload to a connected device and start the serial port monitor application

### Building the firmware for Windows

¯\_(ツ)_/¯

## Contribution

- Create an issue
- Create a merge request for the issue
- Code, test, repeat
- When ready submit your merge request for approval

Pro tip: Use `shipit-cli` (https://gitlab.com/intello/shipit-cli)


